
		 /** Evento Modal **/

		$(document).ready( function(e) {
			$('#botao, #botao2').click(function() {
				$('#modal').fadeIn(500)
				$('#cart-box').fadeIn(500)
			});
			$('.botao3').click(function() {
				$('#modal').fadeOut(500)
			});
			
			$('.carrinho').click(function() {
				$('#cart-box').fadeIn(500)
			});
			$('.botao5').click(function() {
				$('#cart-box').fadeOut(500)
			});

			$('#limpar').click(function() {
				LimparCarrinho();
			});
			$('#botaofrete').click(function() {
				alert('O valor do frete é R$0,00');
			});
			$('.comprar').click(function() {
				alert('Parabéns ! A compra foi finalizada !');
			});
			/** Autocomplete na Pesquisa  **/
	
			$(function() {
				var mario = [
					"Action Figure Mario - Coleção Topzera das Galáxias",
					"Action Figure Luigi - Coleção Topzera das Galáxias",
					"Action Figure Peach - Coleção Topzera das Galáxias",
					"Action Figure Browser - Coleção Topzera das Galáxias"
				];
				$("#Mario" ).autocomplete({
					source: mario
				});
			});

			$('.carousel').slick({
				dots: true,
				infinite: true,
				speed: 500,
				slidesToShow: 4,
				slidesToScroll: 2,
				responsive: [
					{
						breakpoint: 1008,
						settings: {
						  slidesToShow: 2,
						  slidesToScroll: 1
						}
					  },
					{
						breakpoint: 800,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
					  }
					}
				]
			});
		});


		if(localStorage.getItem('itens') !== null) {
				var itens = JSON.parse(localStorage.getItem('itens'));
	
				itens.forEach(function(item){
					AddItemToDOM(prod);
				});
			}
		function LimparCarrinho() {
			localStorage.clear();
			$("#itens").empty();
		}
		

		/* Inclusão de item no Quick Cart */
		function AddCarrinho(produto, qtd, valor) { 
			var itens = [];
			if (localStorage.getItem("itens") !== null) {
				itens = JSON.parse(localStorage.getItem("itens"));
			}
			var prod = { produto: produto, qtd: qtd, valor: valor * qtd };
			localStorage.setItem('itens', JSON.stringify(itens.concat([prod])));
			AddItemToDOM(prod)
		}

		function CalculaTotal() {
			var total = 0.0;
			
			if(!localStorage.getItem('itens')) {
				return total;
			}
			
			var itens = JSON.parse(localStorage.getItem('itens'));
			
			itens.forEach(function(item){
				total += item.valor;
			});
			
			return total;
		}

		function AddItemToDOM(prod) {
			if (!prod) {
				return false;
			}
			
			var el = "<div>";
			el += prod.qtd + " x ";
			el += prod.produto;
			el += " ";
			el += "R$: " + prod.valor + "<hr>";
			el += "</div>";

			$('#itens').append(el);

			var total = CalculaTotal() + parseFloat(prod.valor);
			$("#total").html(total.toFixed(2));
		}

